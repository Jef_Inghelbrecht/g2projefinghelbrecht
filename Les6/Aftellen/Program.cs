﻿using System;

namespace Aftellen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Aftellen");
            // we beginnen met 10 en we tellen af tot 0 en dan zeggen we beep
            // we tonen elke aftelling
            for (int i = 10; i >= 0; --i)
            {
                Console.WriteLine(i);
            }
            Console.Beep();
            Console.ReadKey();

        }
    }
}
