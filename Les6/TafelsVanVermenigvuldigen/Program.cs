﻿using System;

namespace TafelsVanVermenigvuldigen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tafels van vermenigvuldiging");
            // eerste rij, getallen van de vermenigvuldiging
            Console.Write('\t');
            for (int i = 1; i <= 10; i++)
            {
                Console.Write($"{i}\t");
            }
            Console.WriteLine();
            for (int i = 1; i <= 10; i++)
            {
                // binnenste for maakt kolommen
                for (int j = 1; j <= 10; j++)
                {
                    // getallen van de vermenigvuldiging
                    if (j == 1)
                    {
                        Console.Write($"{i}\t");
                    }
                    // Als i = 1, is dat de tafel van 1
                    Console.Write($"{j * i}\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
