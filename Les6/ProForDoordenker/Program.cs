﻿using System;

namespace ProForDoordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pro for doordenker");
            Console.Write("Typ een getal in: ");
            byte breedte = Convert.ToByte(Console.ReadLine());
            // aantal regels printen
            for (byte i = 1; i <= breedte; i++)
            {
                // als de eerste lijn, 1 ster
                // als tweede lijn, 2 ster
                for (byte j = 1; j <= i; j++)
                { 
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            breedte--;
            for (byte i = breedte; i >= 1; i--)
            {
                // als de eerste lijn, breedte sterren
                // als tweede lijn, breedte - 1 ster
                for (byte j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
