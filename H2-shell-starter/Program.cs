﻿using System;

namespace H2_shell_starter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H2-shell-starter");
            System.Diagnostics.Process proces = new System.Diagnostics.Process();
            proces.StartInfo.FileName = @"C:\Users\jefin\OneDrive\AP\ProgrammerenG1\Les5\H4 - schoenverkoper\H4 - schoenverkoper\bin\Debug\netcoreapp3.0\H4 - schoenverkoper.exe";
            proces.StartInfo.UseShellExecute = false;
            proces.StartInfo.RedirectStandardOutput = true;
            proces.StartInfo.RedirectStandardError = true;
            proces.Start(); //start process

            // Read the output (or the error)
            string output = proces.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            string err = proces.StandardError.ReadToEnd();
            Console.WriteLine(err);
            //Continue
            Console.WriteLine("Klaar");
            Console.ReadLine();

        }
    }
}
