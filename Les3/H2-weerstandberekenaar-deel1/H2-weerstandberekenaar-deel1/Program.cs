﻿using System;

namespace H2_weerstandberekenaar_deel1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.Write("Geef de waarde(uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            string ring1 = Console.ReadLine();
            Console.Write("Geef de waarde(uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            string ring2 = Console.ReadLine();
            Console.Write("Geef de waarde(uitgedrukt in een getal van - 2 tot 7) van de derde ring(exponent): ");
            string ring3 = Console.ReadLine();


            // nu gaan we berekenen, dus hebben we gehele getallen nodig
            // het plusteken hieronder betekent concatenation (aan elkaar plakken)
            // de Convert.ToInt32 gaat de aan elkaar geplakte string
            // converteren naar een 32 bit integer (int)
            int getal1 = Convert.ToInt32(ring1 + ring2);
            // Math.Pow methode om het resultaat van de machte van een getal
            // te berekenen 1O^ring3
            int getal2 = Convert.ToInt32(Math.Pow(10, Convert.ToInt32(ring3)));
            Console.WriteLine($"Resultaat is {getal1 * getal2} Ohm, ofwel {getal1}x{getal2}.");
            ConsoleColor achtergrondkleur;
            ConsoleColor kleur;
            if (ring1 == "0")
            {
                achtergrondkleur = ConsoleColor.Black;
                kleur = ConsoleColor.White;
            }
            else if (ring1 == "1")
            {
                achtergrondkleur = ConsoleColor.DarkRed;
                kleur = ConsoleColor.Black;
            } else
            {
                achtergrondkleur = ConsoleColor.DarkRed;
                kleur = ConsoleColor.Black;
            }





            Console.WriteLine("╔═══════════════╦═══════════════╗");
            Console.WriteLine("║     ring 1    ║      ring2    ║");
            Console.WriteLine("╟───────────────╫───────────────╢");
            Console.Write($"║     ");
            Console.BackgroundColor = achtergrondkleur;
            Console.ForegroundColor = kleur;
            Console.Write(ring1);
            Console.ResetColor();
            Console.Write("║     ");
            Console.BackgroundColor = achtergrondkleur;
            Console.ForegroundColor = kleur;
            Console.Write(ring2);
            Console.ResetColor();
            Console.WriteLine("║");
            Console.WriteLine("╚═══════════════╩═══════════════╝");

            Console.WriteLine($"2 tot de 3 macht: {Math.Pow(2, 3)}");
            Console.WriteLine($"2 tot de 16 macht: {Math.Pow(2, 16)}");
            Console.WriteLine($"vierkantswortel uit 8: {Math.Sqrt(8)}");
            Console.WriteLine($"vierkantswortel uit 16: {Math.Sqrt(16)}");

            Console.WriteLine($"Het getal pi: {Math.PI}");
            Console.ReadKey();

        }
    }
}
