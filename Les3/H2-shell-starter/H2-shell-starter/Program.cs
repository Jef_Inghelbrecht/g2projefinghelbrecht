﻿using System;

namespace H2_shell_starter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H2-shell-starter");
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "ipconfig";
            process.StartInfo.Arguments = "/all";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start(); //start process
            // Read the output (or the error)
            string output = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            string err = process.StandardError.ReadToEnd();
            Console.WriteLine(err);
            process.Close();
            Console.WriteLine("Klaar");
            Console.ReadKey();

            process.StartInfo.FileName = "ping";
            process.StartInfo.Arguments = "127.0.0.1";
            process.Start(); //start process

            // Read the output (or the error)
            output = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
            err = process.StandardError.ReadToEnd();
            Console.WriteLine(err);
            process.Close();
            //Continue
            Console.WriteLine("Klaar");
            Console.ReadKey();
        }
    }
}
