﻿using System;

namespace h2_string_interpolation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Oplossing van Ben, Selim");
            int tafel = 411;
            int getal = 1;
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();

            /* ruimte */
            float factor, gewicht = 84f;
            string planeetNaam = "Mercurius";
            factor = 0.38f;
            Console.WriteLine($"Op {planeetNaam} heb je een schijnbaar gewicht van {factor * gewicht} kg.");
        }
    }
}
