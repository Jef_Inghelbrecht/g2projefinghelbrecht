﻿using System;

namespace LerenWerkenMetStringEnChar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Hello World!");
            // in een tekst wil ik een woord tussen dubbele aanhalingstekens plaatsen
            // Bv Jan heeft gewonnen in "Belgium got talent"
            Console.WriteLine("Jan heeft gewonnen in \"Belgium got talent\".");
            // We moeten de dubbele aanhalingsteken die het merk aangeven
            // escapen en laten voorafgaan door een backspace
            //Console.WriteLine("De zon hing laag.\ntussen de witte muren\nverbloedde goud en zwart\nhet avondrood.");
            //Console.WriteLine("Is 62 een A? \x0062");
            //Console.Write("Voornaam: ");        
            //string voornaam = Console.ReadLine();
            //Console.Write("Familienaam: ");
            //string familienaam = Console.ReadLine();
            //Console.Write("Leeftijd: ");
            //string leeftijd = Console.ReadLine();
            //Console.Write("Telefoonnummer: ");
            //string tel = Console.ReadLine();
            //Console.WriteLine($"Je naam is {voornaam} {familienaam}\nJe bent {leeftijd} oud.\nJe bent bereikbaar op {tel}.");
            //string output = String.Format("Je naam is {0} {1}\nJe bent {2} oud.\nJe bent bereikbaar op {3}",
            //        voornaam, familienaam, leeftijd, tel);
            //Console.WriteLine(output);
            Decimal pricePerOunce = 17.36m;
            String s = String.Format("The current price is {0:C2} per ounce.",
                                     pricePerOunce);
            Console.WriteLine(s);
            double getal = 45.34598;
            // Getal tonen als geldeenheid
            // uit het system het geldsymbool opvragen
            string geldEenheid = getal.ToString("C", 
                System.Globalization.CultureInfo.CreateSpecificCulture("eu-EU"));
            // string outputInterpolatie = $"Het bedrag is (:D10): {getal:D2}";
            string outputFormat = String.Format("Het bedrag is {0}", getal);
            // Console.WriteLine(outputInterpolatie);
            Console.WriteLine(outputFormat);
            string rechthoek = $"\u231C";
            Console.WriteLine(rechthoek);
            Console.WriteLine("\u02e7");
            Console.WriteLine("\u231d");
            Console.WriteLine("\u231E");
            Console.WriteLine("\u0231F");

            int max = Math.Max(200, 100);
            Console.WriteLine($"Max getal is: {max}");
            double breuk = 10 / 3;
            double round = Math.Round(breuk, 5);
            Console.WriteLine($"Afgerond getal is: {round}");

            Console.ReadKey();
        }
    }
}
