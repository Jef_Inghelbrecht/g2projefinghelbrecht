﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inghelbrecht
{
    enum PrintType { Int, Ascii, Hex, Binary, SpongeBob };
    class Jef
    {
        public static int[] VulIntArrayAsc(int start, int einde)
        {
            int[] getallen = new int[(einde + 1) - start];
            for (int i = start; i <= einde; i++)
            {
                getallen[i - start] = i;
            }
            return getallen;
        }

        public static int[] VulIntArrayDesc(int einde, int start)
        {
            int[] getallen = new int[(einde + 1) - start];
            for (int i = einde; i >= start; i--)
            {
                getallen[einde - i] = i;
            }
            return getallen;
        }

        // overloading: zelfde methodenaam, andere parameters
        public static int[] VulIntArrayAsc(char start, char einde)
        {
            return VulIntArrayAsc(Convert.ToInt32(start), Convert.ToInt32(einde));
        }

        public static int[] VulIntArrayDesc(char einde, char start)
        {
            return VulIntArrayDesc(Convert.ToInt32(einde), Convert.ToInt32(start));
        }
        // Als type = 1, print getallen, als type = 2 print karakters
        public static string PrintIntArray(int[] getallen, int type)
        {
            string tekst = "De inhoud van de array is:\n";
            for (int i = 0; i <= getallen.Length - 1; i++)
            {
                switch (type)
                {
                    case 1:
                        tekst += $"{getallen[i]}\n";
                        break;
                    case 2:
                        tekst += $"{Convert.ToChar(getallen[i])}\n";
                        break;
                    case 3:
                        tekst += $"{Convert.ToString(getallen[i], 16).ToString()}\n";
                        break;
                    case 4:
                        tekst += $"{Convert.ToString(getallen[i], 2).ToString()}\n";
                        break;
                }
            }
            return tekst;
        }

        public static string PrintIntArray(int[] getallen, PrintType type)
        {
            string tekst = "De inhoud van de array is:\n";
            for (int i = 0; i <= getallen.Length - 1; i++)
            {
                switch (type)
                {
                    case PrintType.Int:
                        tekst += $"{getallen[i]}\n";
                        break;
                    case PrintType.Ascii:
                        tekst += $"{Convert.ToChar(getallen[i])}\n";
                        break;
                    case PrintType.Hex:
                        tekst += $"{Convert.ToString(getallen[i], 16).ToString()}\n";
                        break;
                    case PrintType.Binary:
                        tekst += $"{Convert.ToString(getallen[i], 2).ToString()}\n";
                        break;
                }
            }
            return tekst;
        }

        public static string AdresArray()
        {
            // een adressen array: naam, voornaam, stad
            // we hebben een array waarvan elk element op zijn beurt een array is
            // dus een array waarin we vier adressen met elk drie kenmerken kunnen opslaan
            string[,] adressen = new string[4, 3];
            adressen[0, 0] = "Sarah";
            adressen[0, 1] = "Bettens";
            adressen[0, 2] = "Gent";
            adressen[1, 0] = "Jan"; 
            adressen[1, 1] = "Pieters";
            adressen[1, 2] = "Aalst";
            string adresTekst = string.Empty;
            // string adresTekst = "";
            int maxDimOuter = adressen.GetLength(0);
            int maxDimInner = adressen.GetLength(1);

            for (int i = 0; i < maxDimOuter; i++)
            {
                for (int j = 0; j < maxDimInner; j++)
                {
                    
                    adresTekst += $"{adressen[i,j]}\n";
                }
            }
            return adresTekst;
        }

        public static string Print2DStringArray(string[,] array)
        {
            string adresTekst = string.Empty;
            // string adresTekst = "";
            int maxDimOuter = array.GetLength(0);
            int maxDimInner = array.GetLength(1);

            for (int i = 0; i < maxDimOuter; i++)
            {
                for (int j = 0; j < maxDimInner; j++)
                {

                    adresTekst += $"{array[i, j]} ";
                }
                adresTekst += '\n';
            }
            return adresTekst;
        }

        public static string[,] VulNArrayMetAdressen()
        {
            // door een constante te gebruiken moeten we niet met literals werken
            const int MaxAantalAdressen = 2;
            // een adressen array: naam, voornaam, stad
            // we hebben een array waarvan elk element op zijn beurt een array is
            // dus een array waarin we vier adressen met elk drie kenmerken kunnen opslaan
            string[,] adressen = new string[MaxAantalAdressen, 3];
            int aantaalIngegevenAdressen = 0;
            while (aantaalIngegevenAdressen < MaxAantalAdressen )
            {
                Console.Write("Typ je voornaam in: ");
                string voornaam = Console.ReadLine();
                Console.Write("Typ je familienaam in: ");
                string familienaam = Console.ReadLine();
                Console.Write("Typ je stad in: ");
                string stad = Console.ReadLine();
                adressen[aantaalIngegevenAdressen, 0] = $"<h1>{voornaam}";
                adressen[aantaalIngegevenAdressen, 1] = $"{familienaam}</h1>";
                adressen[aantaalIngegevenAdressen, 2] = $"<address>{stad}</address>";
                aantaalIngegevenAdressen++;
            }
            return adressen;
        }
    }
}
