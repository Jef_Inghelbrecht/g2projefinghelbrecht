﻿using System;
using Inghelbrecht;

namespace LerenWerkenMetArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met arrays");
            //// array vullen met 0 tot 10
            //int[] getallen = new int[11];
            //for (int i = 0; i <= getallen.Length-1; i++)
            //{
            //    getallen[i] = i;
            //}

            //Console.WriteLine("Inhoud van de array:");
            //for (int i = 0; i <= getallen.Length-1; i++)
            //{
            //    Console.WriteLine($"element {i} bevat {i}");
            //}
            //Console.WriteLine("Inhoud van de array in omgekeerde volgorde:");
            //for (int i = getallen.Length-1; i >= 0; i--)
            //{
            //    Console.WriteLine($"element {i} bevat {i}");
            //}
            int[] nieuweArray = Inghelbrecht.Jef.VulIntArrayAsc(200, 400);
            Console.WriteLine(Jef.PrintIntArray(nieuweArray, 1));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc(100, 1), PrintType.Int));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayAsc('a', 'z'), 2));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc('A', '\b'), PrintType.Binary));
            // sentinel
            bool done = false;
            do
            {
                Console.Clear();
                Console.WriteLine("1. getallen tussen 200 en 400");
                Console.WriteLine("2. getallen van  100 naar 1");
                Console.WriteLine("3. letters van a tot z");
                Console.WriteLine("4. letters van A tot \\b");
                Console.WriteLine("5. Een eenvoudige adres n-dimensionele array");
                Console.WriteLine("6. Vraag naam en stad en stop die in een n-dimensionele array");

                Console.WriteLine("0. Stop");

                char keuze = Console.ReadKey().KeyChar;
                switch (keuze)
                {
                    case '1':
                        int[] nogEenArray = Inghelbrecht.Jef.VulIntArrayAsc(200, 400);
                        Console.WriteLine(Jef.PrintIntArray(nogEenArray, 1));
                        break;
                    case '2':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc(100, 1), PrintType.Int));
                        break;
                    case '3':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayAsc('a', 'z'), 2));
                        break;

                    case '4':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc('A', '\b'), PrintType.Binary));
                        break;
                    case '5':
                        Console.WriteLine(Jef.AdresArray());
                        Console.WriteLine("Tik op een toets om verder te gaan.");
                        Console.ReadKey();
                        break;
                    case '6':
                        string[,] adressen = Jef.VulNArrayMetAdressen();
                        string tekstAdres = Jef.Print2DStringArray(adressen);
                        Console.WriteLine(tekstAdres);
                        Console.WriteLine("Tik op een toets om verder te gaan.");
                        Console.ReadKey();
                        break;
                    case '0':
                        done = true;
                        break;
                }
            } while (!done);

            Console.ReadLine();
        }


    }
}
