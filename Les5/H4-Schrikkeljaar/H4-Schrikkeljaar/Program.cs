﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schrikkeljaar");
            // De gebruiker voert een jaartal in en jouw programma toont of het wel of 
            // geen schrikkeljaar is. Een schrikkeljaar is deelbaar door 4, 
            // behalve als het ook deelbaar is door 100, tenzij het wél deelbaar is door 400.
            Console.Write("Voer een jaartal in: ");
            int jaartal = Convert.ToInt32(Console.ReadLine());
            bool isSchrikkeljaar = false;
            if ((jaartal % 4) == 0)
            {
                if ((jaartal % 100) == 0)
                {
                    if ((jaartal % 400) == 0)
                    {
                        isSchrikkeljaar = true;
                    }
                }
                else
                {
                    isSchrikkeljaar = true;
                }
            }
            if (isSchrikkeljaar)
            {
                Console.WriteLine($"Het jaar {jaartal} is schrikkeljaar");
            }
            else {
                Console.WriteLine($"Het jaar {jaartal} is geen schrikkeljaar");
            }
            Console.ReadLine();

        }
    }
}
