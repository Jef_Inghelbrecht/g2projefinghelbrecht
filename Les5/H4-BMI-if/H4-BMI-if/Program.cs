﻿using System;

namespace H4_BMI_if
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H4-BMI-if");
            Console.WriteLine("H3-BMI-berekenaar");
            Console.Write("Je gewicht: ");
            float gewicht = float.Parse(Console.ReadLine());
            Console.Write("Je lengte in meter: ");
            float lengte = float.Parse(Console.ReadLine());
            double bmi = gewicht / Math.Pow(lengte, 2);
            string message = "Geen opmerking";
            if (bmi < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                message = "Ondergewicht";
            }
            else if (bmi >= 18.5 && bmi < 25)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                message = "Normaal gewicht";
            }
            else if (bmi >= 25 && bmi < 30)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                message = "Overgewicht";
            }
            else if (bmi >= 30 && bmi < 40)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                message = "Zwaarlijvig";
            }
            else if (bmi >= 40 )
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                message = "Obesitas";
            }
            Console.WriteLine($"Je BMI is {bmi:F2}");
            Console.WriteLine(message);
            Console.ReadKey();
        }
    }
}
