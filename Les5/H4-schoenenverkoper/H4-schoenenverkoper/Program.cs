﻿using System;

namespace H4_schoenenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoenenverkoper");
            // Hou variabelen bij voor de prijs, de gereduceerde prijs en 
            //het aantal paar dat nodig is om korting te krijgen. 
            // De eerste twee variabelen maak je const.
            const float prijs = 20.00f;
            const float promoPrijs = 10.00f;
            int aantalVoorKorting = 10;
            Console.Write("Hoeveel paar schoenen wens je te kopen? ");
            int aantalPaarSchoenen = Convert.ToInt32(Console.ReadLine());
            Console.Beep();
            if (aantalPaarSchoenen >= aantalVoorKorting)
            {
                Console.WriteLine($"Prijs voor {aantalPaarSchoenen} in promo is {aantalPaarSchoenen * promoPrijs}");
            }
            else
            {
                Console.WriteLine($"Prijs voor {aantalPaarSchoenen} volle pot is {aantalPaarSchoenen * prijs}");

            }
            Console.ReadKey();
        }
    }
}
