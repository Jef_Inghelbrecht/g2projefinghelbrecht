﻿using System;

namespace LerenWerkenMetMethoden
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Maak een keuze!");
            Console.WriteLine("1. Teken driehoek");
            Console.WriteLine("2. Teken rechthoek");
            Console.WriteLine("0. Stop met kiezen en maak scherm leeg.");
            int keuze;
            do
            {
                string input = Console.ReadLine();
                bool success = Int32.TryParse(input, out keuze);
                // als er iets anders dan een getal is ingetypt
                if (!success)
                {
                    keuze = -1; 
                }
                // keuze = Convert.ToByte(Console.ReadLine());
                switch (keuze)
                {
                    case 1:
                        TekenDriehoek();
                        break;
                    case 2:
                        Console.Write("Hoogte van de rechthoek: ");
                        int hoogte = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Lengte van de rechthoek: ");
                        int lengte = Convert.ToInt32(Console.ReadLine());
                        TekenRechthoek(lengte, hoogte);
                        break;
                    case 0:
                        Console.Clear();
                        break;
                    case -1:
                        Console.WriteLine("Je moet een waarde tussen 0 en 2 intypen!");
                        break;
                    default:
                        Console.Beep();
                        Console.WriteLine("Je keuze moet 1, 2 of 0 zijn.");
                        break;
                }
            } while (keuze != 0);
            Console.ReadKey();
        }

        // De code voor de staande driehoek in een methode zetten
        static void TekenDriehoek()
        {
            Console.WriteLine("For doordenker extra: Top kerstboom");
            Console.Write("Geef aantal lijnen van de kersboom op: ");
            byte aantalRijen = Convert.ToByte(Console.ReadLine());
            for (byte j = 1; j <= aantalRijen; j++)
            {
                if (j % 3 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                else if (j % 2 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if (j % 1 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                //Console.WriteLine(j % 1);
                //Console.WriteLine(j % 2);
                //Console.WriteLine(j % 3);
                // print aantalrijen - 1 x een spatie
                // gevolgd 1 ster, en ga naar volgende lijn
                for (byte i = 1; i <= (aantalRijen - j); i++)
                {
                    Console.Write(' ');
                }
                for (byte i = 1; i < j * 2; i++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
        }

        static void TekenRechthoek(int lengte, int hoogte)
        {
            for (int i = 1; i <= hoogte; i++)
            {
                for (int j = 1; j <= lengte; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
        }
    }
}
