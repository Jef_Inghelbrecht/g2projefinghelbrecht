﻿using System;

namespace Programmeren
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Wat is dat eigenlijk een literal?");
            //string titel = "Wat is dat eigenlijk een literal?";
            //Console.WriteLine(titel);
            //// schrijf de code om de tekst "Hello World!" te tonen
            //// zonder dat we een literal gebruiken
            //Console.Write("Typ tekst in dat we moeten tonen: ");
            //titel = Console.ReadLine();
            //Console.WriteLine("De ingetype tekst is: " + titel);
            //// https://www.tutorialspoint.com/csharp/csharp_data_types.htm
            //// een eenvoudige rekenmachine
            //// gehele getallen
            //int a, b;
            //// met literals
            //a = 100;
            //b = 1250000;
            //int som = a + b;
            //Console.WriteLine("De som van " + a + " " + b + " is gelijk aan " + som);
            //// gehele getallen
            //decimal decimalA, decimalB;
            //// met literals
            //decimalA = 100.55m;
            //decimalB = 1250000.33m;
            //decimal decimalSom = decimalA + decimalB;
            //Console.WriteLine("De som van " + decimalA + " " + decimalB + " is gelijk aan " + decimalSom);
            //char z, y, x, w, v, u;
            //z = 'S';
            //y = 'm';
            //x = 'a';
            //w = 'i';
            //v = 'l';
            //u = 'i';
            //Console.Write(z);
            //Console.Write(y);
            //Console.Write(x);
            //Console.Write(w);
            //Console.Write(v);
            //Console.WriteLine(u);

            int getal1, getal2;

            getal1 = 10;
            getal2 = 3;

            decimal mSom ;
            float fSom;
            double dSom;
            int iSom;

            mSom = getal1 / getal2;
            Console.WriteLine(mSom);
            fSom = getal1 / getal2;
            Console.WriteLine(fSom);
            dSom = getal1 / getal2;
            Console.WriteLine(dSom);

            iSom = 10 / 3;
            Console.WriteLine(iSom);
            mSom = 10m / 3m;
            Console.WriteLine(mSom);
            fSom = 10f / 3f;
            Console.WriteLine(fSom);
            dSom = 10d / 3d;
            Console.WriteLine(dSom);




            Console.ReadLine();



        }
    }
}
