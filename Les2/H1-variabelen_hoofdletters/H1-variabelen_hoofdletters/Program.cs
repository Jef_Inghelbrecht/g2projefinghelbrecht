﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // variabele om input van de gebruiker in op de slaan
            string myText;
            Console.Write("Typ een zin in: ");
            myText = Console.ReadLine();
            Console.Write("De ingetypte tekst in hoofdletters is: ");
            string myTextInHoofdletters = myText.ToUpper();
            Console.WriteLine(myTextInHoofdletters);
            // en alles in kleine letters?
            string tekstInKleineLetters = myText.ToLower();
            Console.Write("De ingetypte tekst in kleine letters is: ");
            Console.WriteLine(tekstInKleineLetters);

            // Readkey: leest een karakter in en stopt
            Console.ReadKey();
        }
    }
}
