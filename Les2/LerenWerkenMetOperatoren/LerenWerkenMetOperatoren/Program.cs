﻿using System;

namespace LerenWerkenMetOperatoren
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met operatoren");
           
            int x, y;
            x = 12;
            y = 10;
            // niet de plus om getallen op te tellen
            // maar de plus om strings aan elkaar te plakken
            // x en y worden automatisch omgezet naar een string
            Console.WriteLine("De som is " + x + y);
            // gebruik van ronde haken: evaluatie van een expressie
            // gaat van links naar rechts gebruik ronde haken om
            // voorrang tussen de operatoren aangeven
            Console.WriteLine("Maar deze som is " + (x + y));
            // als enkel de rekenkundige operatoren bekijkt is het resultaat 2
            Console.WriteLine("Het resultaat is " + x + y * 3 / 2);
            // voorrang expliciet maken met haken
            Console.WriteLine(("Het resultaat is " + x) + ((y * 3) / 2));
            Console.WriteLine("Het resultaat is " + (x + ((y * 3) / 2)));

            // modulus
            int resultaat = 6 % 2;
            Console.WriteLine("Rest: " + resultaat);
            resultaat = 187 % 2;
            Console.WriteLine("Rest: " + resultaat);
            // Toepassing van Modulus: bereken als een getal even of oneven is
            // door % 2: als de rest gelijk is aan 0, is het een even getal.
            int rest = 187 % 2;
            if (rest == 0)
                Console.WriteLine("Getal is een even getal".ToUpper());
            else {
                string tekst = "Getal is een oneven getal";
                Console.WriteLine(tekst.ToUpper());
            }; 

            Console.ReadLine();

        }
    }
}
