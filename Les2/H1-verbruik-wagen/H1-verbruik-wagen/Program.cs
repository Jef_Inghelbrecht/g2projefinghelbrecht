﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.Write("Hoeveel liter is er nog aanwezig in de benzinetank? ");
            string aantalLiterinTank1 = Console.ReadLine();
            Console.Write("Hoeveel liter zit er nog in de benzinetank na de rit? ");
            string aantalLiterinTank2 = Console.ReadLine();
            Console.Write("kilometerstand van bij de aanvang van de rit? ");
            string kilometerstand1 = Console.ReadLine();
            Console.Write("kilometerstand nadat de rit werd uitgevoerd? ");
            string kilometerstand2 = Console.ReadLine();
            Console.Write("Verbruik tijdens de rit: ");
            Console.WriteLine(100 * ((Convert.ToDouble(aantalLiterinTank1) - 
                Convert.ToDouble(aantalLiterinTank2) )/ 
                (Convert.ToDouble(kilometerstand2) - Convert.ToDouble(kilometerstand1))));
            Console.ReadKey();
        }
    }
}
