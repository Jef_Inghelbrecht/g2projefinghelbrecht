﻿using System;

namespace H1_maaltafels
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("De manier van Pun");
            int result1, result2, result3, result4, result5,
                result6, result7, result8, result9, result10;
            int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8, i = 9, j = 10;
            int k = 411;

            result1 = a * k;
            Console.WriteLine("1 " + "* " + "411 " + "is " + result1);
            Console.ReadLine();
            Console.Clear();

            result2 = b * k;
            Console.WriteLine("2 " + "* " + "411 " + "is " + result2);
            /* voorbeeldoplossing */
            Console.WriteLine("Oplossing van Ben, Selim");
            int tafel = 411;
            int getal = 1;
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            Console.Write(getal + "*" + tafel + " is " + (tafel * getal++));
            Console.ReadLine();
            Console.Clear();
            /* einde voorbeeldoplossing */
            // Extra
            // ++ secrets
            getal = 1;
            Console.WriteLine("Getal = 1 wat is ++getal? " + ++getal);
            getal = 1;
            Console.WriteLine("Getal = 1 wat is getal++? " + getal++);
            Console.ReadLine();

        }
    }
}
