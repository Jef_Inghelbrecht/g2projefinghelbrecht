﻿using System;

namespace H3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer bedrag in?");
            float bedrag = float.Parse(Console.ReadLine());
            Console.WriteLine($"De poef staat op  euro {bedrag} \nVoer bedrag in? ");
            // bij het getal bedrag tellen we het getal op
            // dat de gebruiker heeft ingevoerd
            bedrag += float.Parse(Console.ReadLine());
            Console.WriteLine($"De poef staat op  euro {bedrag} \nVoer bedrag in? ");
            bedrag += float.Parse(Console.ReadLine());
            Console.WriteLine($"De poef staat op  euro {bedrag} \nVoer bedrag in? ");
            bedrag += float.Parse(Console.ReadLine());
            Console.WriteLine($"De poef staat op  euro {bedrag} \nVoer bedrag in? ");
            bedrag += float.Parse(Console.ReadLine());
            Console.WriteLine("*****************");
            Console.WriteLine($"de poef staat op {bedrag}");
            float afbetaling = bedrag / 10f;
            // we doen iets extra:
            // we berekenen het bedrag van de laatste betaling
            float rest = bedrag % 10f;
            // de apestaart @ zorgt ervoor dat de return en de spaties in de string letterlijk wordt 
            // genomen en je dus geen escape tekens moet gebruiken
            Console.WriteLine(@$"Het totaal van  de poef is {bedrag} euro
Dit zal  {Math.Round(afbetaling,MidpointRounding.AwayFromZero)} afbetalingen vragen 
De laaste keer betaal je {rest:f2}");
            Console.ReadLine();
        }
    }
}
