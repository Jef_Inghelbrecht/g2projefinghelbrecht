﻿using System;

namespace H3_random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Random getal = new Random();
            int toevalsGetal;
            int totaal;

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"voer bedrag in ? \n Automatisch gegenereerd: {toevalsGetal} ");
            totaal = toevalsGetal;
            Console.WriteLine($"de poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"voer bedrag in ? \n Automatisch gegenereerd: {toevalsGetal} ");
            totaal += toevalsGetal;
            Console.WriteLine($"de poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"voer bedrag in ? \n Automatisch gegenereerd: {toevalsGetal} ");
            totaal += toevalsGetal;
            Console.WriteLine($"de poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"voer bedrag in ? \n Automatisch gegenereerd: {toevalsGetal} ");
            totaal += toevalsGetal;
            Console.WriteLine($"de poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"voer bedrag in ? \n Automatisch gegenereerd: {toevalsGetal} ");
            totaal += toevalsGetal;
            Console.WriteLine($"de poef staat op {totaal}");

            Console.WriteLine("******************************");
            float afbetaling = totaal / 10;
            float rest = totaal % 10;
            Console.WriteLine(@$"Het totaal van  de poef is {totaal} euro
Dit zal  {Math.Round(afbetaling, MidpointRounding.AwayFromZero)} afbetalingen vragen
De laaste keer betaal je {rest:f2}");
            Console.ReadLine();
        }
    }
}
