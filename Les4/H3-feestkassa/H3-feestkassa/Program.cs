﻿using System;

namespace H3_feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            const float Frieten = 20f;
            const float Hapjes = 10f;
            const float Ijs = 3f;// pascal notatie
            const float Drank = 2f;// constanten variable , waarde verandert niet.
            float totaal;

            Console.WriteLine("mosselen met frietjes ?");
            totaal = Frieten * float.Parse(Console.ReadLine());
            Console.WriteLine("Koninginnenhapjes?");
            totaal += Hapjes* float.Parse(Console.ReadLine());
            Console.WriteLine("ijsjes ?");
            totaal+= Ijs * float.Parse(Console.ReadLine());
            Console.WriteLine("dranken?");
            totaal += Drank * float.Parse(Console.ReadLine());
           

            
            Console.WriteLine($"Het totaal te betalen bedrag is {totaal} EURO");
            Console.ReadLine();
        }
    }
}
