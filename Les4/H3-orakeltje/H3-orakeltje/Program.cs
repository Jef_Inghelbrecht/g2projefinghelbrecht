﻿using System;

namespace H3_orakeltje
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Random of willekeurige getallen");
            // site met info over random getallen
            // https://docs.microsoft.com/en-us/dotnet/api/system.random?view=netframework-4.8#examples
            // maak een toevalsgenerator
            // een 'machine' die willekeurige getallen maakt
            Random toeval = new Random();
            // maak een willekeurig getal
            int toevalsgetal = toeval.Next(5, 125);
            Console.WriteLine($"Het toevalsgetal tussen 5 en 125 is: {toevalsgetal}");
            Console.ReadKey();
            // maak een volgend wilekeurig getal
            // we laten ons programma met 3 dobbelstenen gooien
            int dobbelsteen1 = toeval.Next(1, 6);
            int dobbelsteen2 = toeval.Next(1, 6);
            int dobbelsteen3 = toeval.Next(1, 6);
            Console.WriteLine($"Je hebt gegooid: {dobbelsteen1} {dobbelsteen2} {dobbelsteen3}");
            Console.ReadKey();
         }
    }
}
