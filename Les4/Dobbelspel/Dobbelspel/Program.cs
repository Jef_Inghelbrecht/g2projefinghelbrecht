﻿using System;

namespace Dobbelspel
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dobbelstenen programma");
            const char linkerbovenhoek = '╔';
            const char rechterbovenhoek = '╗';
            const char linkeronderhoek = '╚';
            const char rechteronderhoek = '╝';
            const char lijnHorizontaal = '═';
            const char lijnVertikaal = '║';
            const string teken = "*";

            Console.Write(linkerbovenhoek);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.WriteLine(rechterbovenhoek);
            Console.Write(lijnVertikaal);
            Console.Write(teken.PadLeft(9));
            Console.WriteLine(lijnVertikaal);
            Console.Write(lijnVertikaal);
            Console.Write(teken.PadLeft(3));
            Console.Write(teken.PadLeft(5));
            Console.WriteLine(lijnVertikaal);
            Console.Write(lijnVertikaal);
            Console.Write("*".PadRight(9));
            Console.WriteLine(lijnVertikaal);
            Console.Write(linkeronderhoek);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.Write(lijnHorizontaal);
            Console.WriteLine(rechteronderhoek);
            Console.ReadKey();

        }
    }
}
