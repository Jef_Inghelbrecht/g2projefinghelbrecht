﻿using System;

namespace H3_geometric_fun
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("H3-geometric-fun");
            Console.Write("Geef een hoek, uitgedrukt in graden: ");
            // Farah stelt de volgende vraag: waarom een string?
            string input = Console.ReadLine();
            // eigenlijk moet dat int zijn, gewoon als voorbeeld
            long graad;
            bool isGetal = Int64.TryParse(input, out graad);
            if (isGetal)
            {
                // doen we de berekeningen
                double radiaal = graad * (Math.PI / 180);
                double sinus = Math.Sin(radiaal);
                double cosinus = Math.Cos(radiaal);
                double tangens = Math.Tan(radiaal);
                
                Console.WriteLine($"{graad} graden is {Math.Round(radiaal, 2):0.00} radialen.");
                Console.WriteLine($"{graad} graden is {Math.Round(graad * (Math.PI / 180), 2):0.00} radialen.");
                Console.WriteLine($"De sinus is {sinus:0.00}."  );
                Console.WriteLine($"De cosinus is {cosinus:0.00}.");
                Console.WriteLine($"De tangens is {tangens:0.00}.");
                // Math.Round formatteert niet
                Console.WriteLine($"De tangens is {Math.Round(tangens, 2)}.");
                Console.ReadKey();
            } else
            {
                Console.WriteLine("Je moet een getal van 0 tot 360 ingeven.");
            }
        }
    }
}
