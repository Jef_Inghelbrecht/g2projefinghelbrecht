﻿using System;

namespace ForDoordenkerExtra
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("For doordenker extra: Top kerstboom");
            Console.Write("Geef aantal lijnen van de kersboom op: ");
            byte aantalRijen = Convert.ToByte(Console.ReadLine());
            for (byte j = 1; j <= aantalRijen; j++)
            {
                if (j % 3 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                } 
                else if (j % 2 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if (j % 1 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                //Console.WriteLine(j % 1);
                //Console.WriteLine(j % 2);
                //Console.WriteLine(j % 3);
                // print aantalrijen - 1 x een spatie
                // gevolgd 1 ster, en ga naar volgende lijn
                for (byte i = 1; i <= (aantalRijen - j); i++)
                {
                    Console.Write(' ');
                }
                for (byte i = 1; i < j*2; i++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
